#library(inlabru)
#library(devtools)
#install.packages("devtools")
#library(INLAutils)
#library(INLA)
#install.packages("INLA",repos=c(getOption("repos"),INLA="https://inla.r-inla-download.org/R/stable"), dep=TRUE)
#Install INLAUtils
#install_github('timcdlucas/INLAutils')
library(INLA)
library(INLAutils)
library(devtools)
library(sp)
library(spgwr)
library(ggplot2)
library(sf)
library(spatial)
library(spData)
library(rgdal)
library(spdep)
#library(tmap)
#library(MASS)
library(gstat)
library(geoR)
#library(sgeostat)
#library(geospt)
#library(xtable)
#library(RGeostats)
#library(intamap)
#library(maptools)
#library(maps)

library(ggspatial)
#library(corrplot)
#library(gridExtra)
#library(sqldf)
#library(maptools)
library(spatstat)




################################################
################################################
###### INTERPOLACION TOPOGRAF�A ################
################################################


setwd("D:/jromero/TesisMaestria/Eventos Sismicos/REPOSITORIO/tesismaestria")

############################################################################
######################## BORDE SUPERIOR ####################################
############################################################################

##Datos Altura obtenidos de TOPEX POSEIDON usando una extensi�n superior al borde 
##de colombia, para evitar problemas de frontera
DframeSpatialTopoR <-readOGR("Shapefile/TopografiaBordeSuperior.shp")

##Zona de estudio con extensi�n en los bordes para generar la interpolaci�n de puntos
ShapeZona<-readOGR("Shapefile/BordeSuperior.shp")

epsg3116<-"+init=epsg:21897"
ShapeZona<-spTransform(ShapeZona,CRS(epsg3116))

#PuntosInterpolar<- spsample(ShapeZona,n=70000,type="regular")
#Se crea malla de interpolaci�n 
PuntosInterpolar = spsample(ShapeZona, cellsize=c(2000,2000), type="regular")
#PuntosInterpolar<- spsample(ShapeZona,n=70000,type="regular")


x11()
plot(PuntosInterpolar)


################################################
################################################
### ANALISIS DE NORMALIDAD TOPOGRAFIA #################
################################################
Mystd<-function(x) {(x-mean(x))/sd(x)}
hist(DframeSpatialTopoR$Topo) ##No normalidad
hist(log(DframeSpatialTopoR$Topo)) ##No normalidad

#Toposelect = DframeSpatialTopoR[,c("Longitud","Latitud","Topo")] 
#Topo.rgdb <- db.create(Toposelect,ndim=2,autoname=F)
#Topo.herm <- anam.fit(Topo.rgdb,name="Topo",type="gaus")
#Topo.hermtrans <- anam.z2y(Topo.rgdb,names="Topo",anam=Topo.herm)
#Topo.trans <- Topo.hermtrans@items$Gaussian.Topo


DframeSpatialTopoR$TopoAnamorfosis<-Mystd(DframeSpatialTopoR$Topografia)


#############################################################
#######CONSTRUCCI�N DE LA MEJOR MALLA #######################
#############################################################
Coordenadas<-data.frame(DframeSpatialTopoR$Longitud,DframeSpatialTopoR$Latitud)
coordinates(Coordenadas)=~DframeSpatialTopoR.Longitud+DframeSpatialTopoR.Latitud
proj4string(Coordenadas)<-CRS("+init=epsg:21897")




#############################################################
####CONSIDERANDO una cobertura no convexa ##################



MeshNoConvex <- inla.nonconvex.hull(Coordenadas)

MeshNoConvex1<-inla.mesh.2d(
  boundary = MeshNoConvex, max.edge = c(80000,  350000),offset = c(50000,150000), min.angle = c(26,16), cutoff = 0,plot.delay = NULL
)
plot(MeshNoConvex1)


####construccion SPDE COVARIANZA MATHERN
TopoSPDEmaterMesNohConvex1 <- inla.spde2.matern(mesh = MeshNoConvex1, alpha = 2,constr = TRUE) ##Modelo Mathern

#Luego generamos el conjunto de �ndices para el modelo SPDE
#Especificamos el nombre del efecto ( s), s es GAUSS y el n�mero de v�rtices en el modelo SPDE

s.indexMeshNoConvex1 <- inla.spde.make.index(name="spatial.field",n.spde=TopoSPDEmaterMesNohConvex1$n.spde)

#DATOS DE PREDICCION

s.proyeccionMeshNoConvex1 <- inla.spde.make.A(mesh = MeshNoConvex1, loc = Coordenadas)

s.prediccionMeshNoConvex1<-inla.spde.make.A(mesh = MeshNoConvex1, loc = PuntosInterpolar)
DframeSpatialTopoRdf<-data.frame(DframeSpatialTopoR)
###########################################
###MODELO CONN COVARIABLES #################

stk.MeshNoConvex1 <- inla.stack(
  data = list(Y.TOPO =  DframeSpatialTopoR$Topografia),
  A = list(s.proyeccionMeshNoConvex1,1),
  effects = list(c(s.indexMeshNoConvex1, list(Intercept = 1)),
                 list(dist = DframeSpatialTopoR$Latitude)),
  tag = "meuse.data")

formMeshNoConvex1 <- Y.TOPO ~ -1 + Intercept  + f(spatial.field, model = TopoSPDEmaterMesNohConvex1)
PuntosInterpolardf=data.frame(PuntosInterpolar)
TOPO.stackpredict <- inla.stack(
  data = list(Y.TOPO = NA),
  A = list(s.prediccionMeshNoConvex1,1),
  effects = list(c(s.indexMeshNoConvex1, list (Intercept = 1)),list=(dist=PuntosInterpolardf[,2])),
  tag = "pred")

Union_stack <- inla.stack(stk.MeshNoConvex1,TOPO.stackpredict)

TOPOINLAMeshNoConvex1 <- inla(formMeshNoConvex1 , family = 'gaussian',
                              data = inla.stack.data(Union_stack, spde =TopoSPDEmaterMesNohConvex1,compute=TRUE),
                              control.predictor = list(A = inla.stack.A(Union_stack)),
                              control.compute = list(dic=TRUE, waic=TRUE,cpo=TRUE,config = TRUE),verbose = FALSE)



IndexPredic <- inla.stack.index(Union_stack, tag = "pred")$data
pred_mean2 <-TOPOINLAMeshNoConvex1$summary.fitted.values[IndexPredic, "mean"]
sd<-sd(DframeSpatialTopoR$Topografia)
prom<-mean(DframeSpatialTopoR$Topografia)
Prediccion<-pred_mean2*sd+prom

summary(Prediccion)
summary(pred_mean2)
summary(DframeSpatialTopoR$Topografia)


dpm <- rbind(
  data.frame(
    Este = PuntosInterpolar$x1, Norte = PuntosInterpolar$x2,
    value = pred_mean2, variable = ""
  ))
dpm$variable <- as.factor(dpm$variable)
library(raster)
epsg3116<-"+init=epsg:3116"
coordinates(dpm)<-~Este+Norte
gridded(dpm) <- TRUE
dfr <- rasterFromXYZ(dpm, crs=epsg3116)
plot(dfr)
writeRaster(dfr, filename="D:/jromero/TesisMaestria/Eventos Sismicos/REPOSITORIO/tesismaestria/Historicos/TOPEX/TopografiaBordeSuperior.tif", options=c('TFW=YES'),overwrite=TRUE)

res(dfr)
Prueba<-sqldf("SELECT * FROM DframeSpatialTopoR
              WHERE Topo>3851")


############################################################################
######################## BORDE INFERIOR ####################################
############################################################################

DframeSpatialTopoRInf <-readOGR("Shapefile/TopografiaBordeInferior.shp")
DframeSpatialTopoRInf$TopoAnamorfosis<-Mystd(DframeSpatialTopoRInf$Topografia)

ShapeZonaInf<-readOGR("Shapefile/BordeInferior.shp")

epsg3116<-"+init=epsg:21897"
ShapeZonaInf<-spTransform(ShapeZonaInf,CRS(epsg3116))
PuntosInterpolar = spsample(ShapeZonaInf, cellsize=c(2000,2000), type="regular")
#PuntosInterpolar<- spsample(ShapeZona,n=70000,type="regular")
#Se crea malla de interpolaci�n 
#PuntosInterpolarInf = spsample(ShapeZonaInf, cellsize=c(1000,1000), type="regular")
#############################################################
#######CONSTRUCCI�N DE LA MEJOR MALLA #######################
#############################################################
CoordenadasInf<-data.frame(DframeSpatialTopoRInf$Longitud,DframeSpatialTopoRInf$Latitud)
coordinates(CoordenadasInf)=~DframeSpatialTopoRInf.Longitud+DframeSpatialTopoRInf.Latitud
proj4string(CoordenadasInf)<-CRS("+init=epsg:21897")







#############################################################
####CONSIDERANDO una cobertura no convexa ##################



MeshNoConvexInf <- inla.nonconvex.hull(CoordenadasInf)

MeshNoConvex1Inf<-inla.mesh.2d(
  boundary = MeshNoConvexInf, max.edge = c(80000,  350000),offset = c(50000,150000), min.angle = c(26,16), cutoff = 0,plot.delay = NULL
)
plot(MeshNoConvex1Inf)


####construccion SPDE COVARIANZA MATHERN
TopoSPDEmaterMesNohConvex1Inf <- inla.spde2.matern(mesh = MeshNoConvex1Inf, alpha = 2,constr = TRUE) ##Modelo Mathern

#Luego generamos el conjunto de �ndices para el modelo SPDE
#Especificamos el nombre del efecto ( s), s es GAUSS y el n�mero de v�rtices en el modelo SPDE

s.indexMeshNoConvex1Inf <- inla.spde.make.index(name="spatial.field",n.spde=TopoSPDEmaterMesNohConvex1Inf$n.spde)

#DATOS DE PREDICCION

s.proyeccionMeshNoConvex1Inf <- inla.spde.make.A(mesh = MeshNoConvex1Inf, loc = CoordenadasInf)

s.prediccionMeshNoConvex1Inf<-inla.spde.make.A(mesh = MeshNoConvex1Inf, loc = PuntosInterpolar)

DframeSpatialTopoRdf<-data.frame(DframeSpatialTopoRInf)
###########################################
###MODELO CONN COVARIABLES #################

stk.MeshNoConvex1 <- inla.stack(
  data = list(Y.TOPO =  DframeSpatialTopoRInf$Topografia),
  A = list(s.proyeccionMeshNoConvex1Inf,1),
  effects = list(c(s.indexMeshNoConvex1Inf, list(Intercept = 1)),
                 list(dist = DframeSpatialTopoRInf$Latitude)),
  tag = "meuse.data")

formMeshNoConvex1 <- Y.TOPO ~ -1 + Intercept  + f(spatial.field, model = TopoSPDEmaterMesNohConvex1Inf)
PuntosInterpolardf=data.frame(PuntosInterpolar)
TOPO.stackpredict <- inla.stack(
  data = list(Y.TOPO = NA),
  A = list(s.prediccionMeshNoConvex1Inf,1),
  effects = list(c(s.indexMeshNoConvex1Inf, list (Intercept = 1)),list=(dist=PuntosInterpolardf[,2])),
  tag = "pred")

Union_stack <- inla.stack(stk.MeshNoConvex1,TOPO.stackpredict)

TOPOINLAMeshNoConvex1 <- inla(formMeshNoConvex1 , family = 'gaussian',
                              data = inla.stack.data(Union_stack, spde =TopoSPDEmaterMesNohConvex1Inf,compute=TRUE),
                              control.predictor = list(A = inla.stack.A(Union_stack)),
                              control.compute = list(dic=TRUE, waic=TRUE,cpo=TRUE,config = TRUE),verbose = FALSE)



IndexPredic <- inla.stack.index(Union_stack, tag = "pred")$data
pred_mean2 <-TOPOINLAMeshNoConvex1$summary.fitted.values[IndexPredic, "mean"]
sd<-sd(DframeSpatialTopoRInf$Topografia)
prom<-mean(DframeSpatialTopoRInf$Topografia)
Prediccion<-pred_mean2*sd+prom

summary(Prediccion)
summary(pred_mean2)
summary(DframeSpatialTopoRInf$Topografia)


dpm <- rbind(
  data.frame(
    Este = PuntosInterpolar$x1, Norte = PuntosInterpolar$x2,
    value = pred_mean2, variable = ""
  ))
dpm$variable <- as.factor(dpm$variable)
library(raster)
epsg3116<-"+init=epsg:3116"
coordinates(dpm)<-~Este+Norte
gridded(dpm) <- TRUE
dfr <- rasterFromXYZ(dpm, crs=epsg3116)
plot(dfr)
writeRaster(dfr, filename="D:/jromero/TesisMaestria/Eventos Sismicos/REPOSITORIO/tesismaestria/Historicos/TOPEX/TopografiaBordeInferior.tif", options=c('TFW=YES'),overwrite=TRUE)
